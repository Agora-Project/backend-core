// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by backend-core.js.
import { name as packageName } from "meteor/backend-core";

// Write your tests here!
// Here is an example.
Tinytest.add('backend-core - example', function (test) {
  test.equal(packageName, "backend-core");
});
