acceptActivityPermitted = function(activity, user) {
    let follower;

    switch (typeof activity.object) { //This should be a follow activity ID.
        case 'string':
            follower = Actors.findOne({id: activity.object});
            break;
        case 'object':
            follower = Actors.findOne({id: activity.object.actor});
            break;
    }

    if (!follower)
        throw new Meteor.Error('Actor not found!', 'No actor with the given ID could be found: ' + activity.object);

    const followee = Actors.findOne({id: activity.actor});

    let pendingFollow = PendingFollows.findOne({actor: follower.id, object: followee.id});

    if (!pendingFollow)
        throw new Meteor.Error('No follow pending!', 'No pending follow between those actors was found: ' + activity.object + ", " + activity.actor);

    return true;
}

processAcceptActivity = function(activity) {

    let follower;

    switch (typeof activity.object) { //This should be a follow activity ID.
        case 'string':
            follower = Actors.findOne({id: activity.object});
            break;
        case 'object':
            follower = Actors.findOne({id: activity.object.actor});
            break;
    }

    const followee = Actors.findOne({id: activity.actor});

    let pendingFollow = PendingFollows.findOne({actor: follower.id, object: followee.id});

    FollowingLists.update({id: follower.following}, {$inc: {totalItems: 1}, $push: {orderedItems: followee.id}});

    if (followee.local)
        FollowerLists.update({id: followee.followers}, {$inc: {totalItems: 1}, $push: {orderedItems: follower.id}});

    PendingFollows.remove(pendingFollow);

    return activity;
};
