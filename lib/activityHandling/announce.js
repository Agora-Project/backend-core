announceActivityPermitted = function(activity, user) {
    if (!Activities.findOne({actor: activity.actor, type: "Announce", object: activity.object}, {fields: {id: true}})) return true;
    else throw new Meteor.Error('Post Already Shared!', 'You have already shared that post!');
}

announceLocalPost = function(activity, post) {
    if (!ShareLists.findOne({id: post.shares})) ShareLists.insert({
        id: post.shares,
        type: "OrderedCollection",
        totalItems: 0,
        orderedItems: []
    });

    ShareLists.update({id: post.shares}, {$inc: {totalItems: 1}, $push: {orderedItems: activity.id}});
}

processAnnounceActivity = function(activity) {

    let post = getPostFromActivity(activity);

    if (post.local) announceLocalPost(activity, post);

    return activity;
};
