const {promisify} = require('util');

webfinger = require('webfinger');

federatedCreateActivityPermitted = function(activity, user) {
    if (Posts.findOne({id: activity.object.id}))
        throw new Meteor.Error('Post Already Exists', 'Cannot insert post, id already exists.');
    if (activity.object.attributedTo !== activity.actor)
        throw new Meteor.Error('Wrong Actor!', 'Posts must be attributed to the same actor that created them.');
}

const findActorByMention = function(mention, callback) {
    if (mention[0] === '@')
        mention = mention.substring(1, mention.length);
    let components = mention.split("@");

    let actor;
    if (components.length === 1 || process.env.ROOT_URL === "http://" + components[1] + "/") {

        actor = Actors.findOne({preferredUsername: components[0], local: true});
        if (actor) {
            if (callback) callback(null, actor);
            else return actor;
        }
    } else {
        actor = Actors.findOne({preferredUsername: components[0], host: components[1]});
        if (actor) {
            if (callback) callback(null, actor);
            else return actor;
        }
        else {
            webfinger.webfinger(mention, Meteor.bindEnvironment(function(err, result) {
                if (result) {
                    let actorID;
                    for (let link of result.links) {
                        if (link.rel === 'self') {
                            actorID = link.href;
                            break;
                        }
                    }

                    importActivityJSONFromUrl(actorID, callback);
                }
            }));
        }
    }
}

promiseActorByMention = promisify(findActorByMention);

processClientPost = function(post, callback) {
    post.content = post.source.content;
    post.summary = post.source.summary;
    post.published = Date.now();
    if (!post.tag) post.tag = [];

    let mentions = post.content.match(/(@\w[\w\.]*\w+)((@\w+(\.\w+)*)(\:\d{0,4})?)?/gi);
    let promisedMentions = [];

    if (mentions)
        for (let mention of mentions) {
            promisedMentions.push(promiseActorByMention(mention));
        }

    Promise.all(promisedMentions).then(function(actors) {
        let i = 0;
        for (let actor of actors) {
            post.content = post.content.replace(mentions[i], '<a href=\"' + actor.url + '\" class=\"u-url mention\">@<span>' + actor.preferredUsername + '</span></a>');
            post.tag.push({
                type: "Mention",
                href: actor.url,
                name: mentions[i]
            });
            if(post.to.indexOf(actor.id) === -1) post.to.push(actor.id);
            i++;
        }

        callback(null, post);
    }).catch(function(err) {
        console.log(err);
    });

}

encapsulateContentWithCreate = function(post) {

    let activity = new ActivityPubActivity("Create", post.attributedTo, post);
    activity.published = post.published;
    return activity;
};

processClientCreateActivity = promisify(function(activity, callback) {
    let post = activity.object;

    //Don't allow posts with no content.
    if (!post.source.content || post.source.content.length < 1)
        throw new Meteor.Error('No content!', 'Cannot insert post without content!');

    //The constants below are from lib/collections/posts.js

    //Don't allow posts with too much content
    if (post.source.content.length > Posts.POST_CONTENT_CHARACTER_LIMIT)
        throw new Meteor.Error('Too much content!', 'Cannot insert post with content greater than ' + Posts.POST_CONTENT_CHARACTER_LIMIT + ' characters!');

    //Don't allow posts with summariesw that are too long.
    if (post.source.summary && post.source.summary.length > Posts.POST_SUMMARY_CHARACTER_LIMIT)
        throw new Meteor.Error('Summary too long!', 'Cannot insert post with summary greater than ' + Posts.POST_SUMMARY_CHARACTER_LIMIT + ' characters!');

    if (post.source.content.length > Posts.POST_CONTENT_SUMMARY_REQUIREMENT && (!post.source.summary || post.source.summary.length < 1))
        throw new Meteor.Error('Summary needed!', 'Posts with more than ' + Posts.POST_CONTENT_SUMMARY_REQUIREMENT + ' characters of content must have a summary!');

    //Don't allow posts that target posts that don't exist.
    if (post.inReplyTo && !Posts.findOne({id: post.inReplyTo}))
        throw new Meteor.Error('Target invalid', 'Targeted post not found!');

    post.local = true;

    processClientPost(post, function(err, result) {
        let post_ID = Posts.insert(result);
        activity.object = Posts.findOne({_id: post_ID});

        delete activity.object.local;

        callback(err, activity);
    });
});

processFederatedCreateActivity = function(activity) {
    const post = getPostFromActivity(activity);

    if (Posts.findOne({id: post.id}))
        throw new Meteor.Error('Post Already Exists', 'Cannot insert post, id already exists: ' + JSON.stringify({prev: Posts.findOne({id: post.id}), post: post}));

    post.local = false;
    Posts.insert(post);

    return activity;
};
