deletePost = function(postID) {
    let post = Posts.findOne({id: postID});

    //Check to make sure the post exists before attempting to delete it.
    if (post === undefined) {
        throw new Meteor.Error('post-not-found', 'No such post was found: ' + postID);
    }

    let target = Posts.findOne({id: post.inReplyTo});

    //Delete all references to the post, then it.
    ReplyLists.update({id: target.replies}, {$inc: {totalItems: -1}, $pull: {orderedItems: post.id}});
    Posts.update({inReplyTo: post.id}, {$unset: {inReplyTo:1}}, {multi: true});
    ReplyLists.remove({id: post.replies});
    Posts.remove({id: postID});
}

processClientDeleteActivity = function(activity) {
    const postID = activity.object;

    deletePost(postID);

    return activity;
};

processFederatedDeleteActivity = function(activity) {
    const postID = activity.object;

    deletePost(postID);

    return activity;
};
