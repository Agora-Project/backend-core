followActivityPermitted = function(activity, user) {
    if (!Actors.findOne({id: activity.object}))
        throw new Meteor.Error('Actor not found!', 'No actor with the given ID could be found: ' + activity.object);

    if (!Activities.findOne({actor: activity.actor, type: "Follow", object: activity.object}, {fields: {id: true}})) return true;
    else throw new Meteor.Error('Already following!', 'You are already following that actor!');
}

acceptFollowActivity = function(activity) {
    let accept = new ActivityPubActivity("Accept", activity.object, activity);
    accept.to.push(activity.actor);
    Meteor.setTimeout(function(){
        dispatchActivity(accept);
    }, 0);
    processAcceptActivity(accept);
}

processClientFollowActivity = function(activity) {

    let follower = Actors.findOne({id: activity.actor});

    let followee = Actors.findOne({id: activity.object});

    if (!PendingFollows.findOne({actor: follower.id, object: followee.id})) {
        PendingFollows.insert(activity);
    }

    if (followee.local) {
        acceptFollowActivity(activity);
    }

    return activity;
};

processFederatedFollowActivity = function(activity) {
    acceptFollowActivity(activity);

    return activity;
};
