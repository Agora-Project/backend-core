likeActivityPermitted = function(activity, user) {
    if (!Activities.findOne({actor: activity.actor, type: "Like", object: activity.object}, {fields: {id: true}})) return true;
    else throw new Meteor.Error('Post Already Liked!', 'You have already liked that post!');
}

likeLocalPost = function(activity, post) {
    if (!LikeLists.findOne({id: post.likes})) LikeLists.insert({
        id: post.likes,
        type: "OrderedCollection",
        totalItems: 0,
        orderedItems: []
    });

    LikeLists.update({id: post.likes}, {$inc: {totalItems: 1}, $push: {orderedItems: activity.id}});
}

processLikeActivity = function(activity) {

    let post = getPostFromActivity(activity);

    if (post.local)
        likeLocalPost(activity, post);

    let actor = Actors.findOne({id: activity.actor});

    if (actor.local)
        ActorLikedLists.update({id: actor.liked}, {$inc: {totalItems: 1}, $push: {orderedItems: activity.id}});

    return activity;
};
