undoActivityPermitted = function (activity, user) {
    let targetActivity = Activities.findOne({id: activity.object});

    if (!targetActivity) {
        throw new Meteor.Error('Activity not found!', 'No activity with the given ID could be found: ' + activity.object);
    }

    if (activity.actor !== targetActivity.actor) {
        throw new Meteor.Error('Activity not owned!', "You can't undo other peoples activities.");
    }

    return true;
};

let processUndoFollowActivity = function(activity, targetActivity) {
    const follower = Actors.findOne({id: targetActivity.actor});

    const followee = Actors.findOne({id: targetActivity.object});

    FollowingLists.update({id: follower.following}, {$inc: {totalItems: -1}, $pull: {orderedItems:  followee.id}});

    if (followee.local)
        FollowerLists.update({id: followee.followers}, {$inc: {totalItems: -1}, $pull: {orderedItems: follower.id}});

    PendingFollows.remove({actor: follower.id, object: followee.id});

    return activity;
}

let processUndoLikeActivity = function(activity, targetActivity) {
    let post = getPostFromActivity(targetActivity);

    if (post.local)
        LikeLists.update({id: post.likes}, {$inc: {totalItems: -1}, $pull: {orderedItems: targetActivity.id}});

    let actor = Actors.findOne({id: targetActivity.actor});

    ActorLikedLists.update({id: actor.liked}, {$inc: {totalItems: -1}, $pull: {orderedItems: targetActivity.id}});

    return activity;
}



let processUndoAnnounceActivity = function(activity, targetActivity) {
    let post = getPostFromActivity(targetActivity);

    if (post.local)
        ShareLists.update({id: post.shares}, {$inc: {totalItems: -1}, $pull: {orderedItems: targetActivity.id}});

    return activity;
}

processUndoActivity = function(activity) {
    let targetActivity = Activities.findOne({id: activity.object});

    switch (targetActivity.type) {
        case "Follow":
            activity = processUndoFollowActivity(activity, targetActivity);
            break;
        case "Like":
            activity = processUndoLikeActivity(activity, targetActivity);
            break;
        case "Announce":
            activity = processUndoAnnounceActivity(activity, targetActivity);
            break;
    }

    Activities.remove({id: targetActivity.id});

    return activity;
};
