const userActivityPermitted = function(user, activity) {
    if (!user) {
        throw new Meteor.Error('Not-logged-in', 'The user must be logged in to perform activities.');
    }

    if (activity.actor !== user.actor)
        throw new Meteor.Error('Actor mismatch!', 'Method actor does not match activity actor!');

    if (!Actors.findOne({id: activity.actor}))
        throw new Meteor.Error('Actor not found!', 'No actor with the given ID could be found in the database: ' + activity.actor);

    return true;
};

getObjectFromActivity = function(activity, collection = Posts) {
    switch (typeof activity.object) {
        case 'string':
            return collection.findOne({id: activity.object});
            break;
        case 'object':
            return activity.object
            break;
    }
};

getPostFromActivity = function(activity) {
    let post = getObjectFromActivity(activity, Posts);

    if (!post)
        throw new Meteor.Error('Post Not Present', 'That post could not be found!');
    else return post;
}

updateOrDeleteActivityPermitted = function(activity, user) {
    const object = getObjectFromActivity(activity, Posts);

    if (activityPubContentTypes.includes(object.type)) {
        const originalObject = Posts.findOne({id: object.id});

        if (!originalObject) throw new Meteor.Error('Post Not Present', "That post is not present in this forum: " + JSON.stringify(activity));

        //Don't allow non-moderators to edit other peoples posts.
        if (activity.actor !== originalObject.attributedTo && (!user || !Roles.userIsInRole(user._id, ['moderator']))) {
            throw new Meteor.Error('Post Not Owned', "Only moderators may edit or delete posts they don't own.");
        }
    } else if (activityPubActorTypes.includes(object.type)) {
        const originalActor = Actors.findOne({id: object.id});

        if (!originalActor) throw new Meteor.Error('Actor Not Present', "That actor is not present in this forum: " + JSON.stringify(activity));

        //Don't allow non-moderators to edit other peoples posts.
        if (activity.actor !== originalActor.id && (!user || !Roles.userIsInRole(user._id, ['moderator']))) {
            throw new Meteor.Error('Actor Not Owned', "Only moderators may edit or delete actors they don't own.");
        }
    }

    return true;
};

const userVerified = function(user) {
    if (!user.emails || user.emails.length < 1 || !user.emails[0].verified) {
        return false;
    }

    return true;
}

const clientActivityPermitted = function(activity, user) {

    userActivityPermitted(user, activity);

    switch(activity.type) {

        case 'Undo':
            return undoActivityPermitted(activity, user);
        //Users can follow and unfollow without being verified. Thus, return here, instead of further down after the verification check.
        case 'Follow':
            return followActivityPermitted(activity, user);;
        case 'Accept':
            return acceptActivityPermitted(activity, user);
        case 'Like':
            return likeActivityPermitted(activity, user);
        case 'Update':
        case 'Delete':
            if (!updateOrDeleteActivityPermitted(activity, user)) return false;
            //No break here, as update and delete activities should be subject to the same restrictions as create and announce.
        case 'Create':
        case 'Announce':
            //Don't allow banned users to post, announce, update, or delete.
            if (user.isBanned) {
                throw new Meteor.Error('Banned', 'Banned users may not perform that activity.');
            }
    }

    //Don't allow unverified users to manipulate the forum. They can still follow people though,
    //which is why follows return above and don't execute this code.
    if(!userVerified(user))
        throw new Meteor.Error('Unverified', 'Unverified users may not perform that activity.');

    return true;
};

cleanActivityPub = function(object) {
    delete object._id;
    delete object.local;
    delete object.undone;

    if (!object['@context']) object['@context'] = "https://www.w3.org/ns/activitystreams";

    if (object.object && typeof object.object === 'object') {
        cleanActivityPub(object.object);
    }

    if (object.orderedItems) {
        for (let i = 0; i < object.orderedItems.length; i++) {
            object.orderedItems[i] = cleanActivityPub(object.orderedItems[i]);
        }
    }

    return object;
};

processClientActivity = async function(user, object) {

    //Set the object as being published right now.
    object.published = new Date().toISOString();

    let activity;
    //We may or may not have an activity to work with here.
    if (!activityPubActivityTypes.includes(object.type)) {

        //If we don't and it's a content object, encapsulate the object in a create activity.
        if (activityPubContentTypes.includes(object.type))
            activity = encapsulateContentWithCreate(object);

        //If we don't, and we don't know what it is, throw an error.
        else throw new Meteor.Error('Unknown type!', 'Cannot handle that type of object!');
    } else {
        //If we do have an activity, proceed normally.
        activity = object;
    }

    if (!clientActivityPermitted(activity, user))
        throw new Meteor.Error('Activity Not Permitted!', 'You are not permitted to perform that activity!');

    activity.local = true;

    let _id = Activities.insert(activity);
    activity = Activities.findOne({_id: _id});

    switch(activity.type){
        case 'Create':
            activity = await processClientCreateActivity(activity);
            break;
        case 'Delete':
            activity = processClientDeleteActivity(activity);
            break;
        case 'Follow':
            activity = processClientFollowActivity(activity);
            break;
        case 'Update':
            activity = processClientUpdateActivity(activity);
            break;
        case 'Like':
            activity = processLikeActivity(activity);
            break;
        case 'Announce':
            activity = processAnnounceActivity(activity);
            break;
        case 'Undo':
            activity = processUndoActivity(activity);
            break;
    }

    Activities.update({id: activity.id}, activity);

    //The setTimeout here is to make the dispatch happen as a separate process, so
    //it doesn't interfere with the rest of the function if it encounters an error.
    Meteor.setTimeout(function() {
         dispatchActivity(activity);
    }, 0);

    return activity;
};
