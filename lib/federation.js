
const dispatchToActor = function(actor, activity) {

    if (!actor || actor.local || !activity || !activity.id) return;

    HTTP.post(actor.inbox, {data: activity, npmRequestOptions: {
        httpSignature: {
            authorizationHeaderName: "Signature",
            keyId: activity.actor + "#main-key",
            key: Keys.findOne({id: activity.actor + "#main-key"}, {fields: {privateKeyPem: 1}}).privateKeyPem
        }
    }}, function(err, result) {
        //if (err) console.log("Error: ", err);
        //if (result) console.log("Result: ", result);
    });
};

dispatchActivity = function(activity) {

    activity = cleanActivityPub(activity);

    if (!activity.id) {
        const _id = Activities.insert(activity);

        activity = Activities.findOne({_id: _id});
    }

    const targetArrays = ['to', 'cc', 'bto', 'bcc', 'audience'];

    for (let i = 0; i < targetArrays.length; i++) {
        const arrayName = targetArrays[i];
        const targetArray = activity[arrayName];
        for (let j = 0; j < targetArray.length; j++) {
            const targetID = targetArray[j];
            if (targetID === "https://www.w3.org/ns/activitystreams#Public")
                continue;
            let actor = Actors.findOne({id: targetID});
            if (actor)
                dispatchToActor(actor, activity);
            else {
                const list = FollowerLists.findOne({id: targetID});
                if (list)
                    for (let actorID of list.orderedItems) {
                        dispatchToActor(Actors.findOne({id: actorID}), activity);
                    }
            }
        }
    }
};

const federatedActivityPermitted = function(activity) {

    //TODO: Check to see if foreign instances are blocked or banned.

    const actor = Actors.findOne({id: activity.actor});

    if (!actor)
        throw new Meteor.Error('Actor Not Found', 'No actor with the given ID could be found in the database: ' + activity.actor);

    switch(activity.type) {

        //Users can follow without being verified. Thus, return here, instead of further down after the verification check.
        case 'Follow':
            return followActivityPermitted(activity, user);
        case 'Like':
        case 'Undo':
            return;
        case 'Accept':
            return acceptActivityPermitted(activity, user);
        case 'Update':
        case 'Delete':
            return updateOrDeleteActivityPermitted(activity, user);
        case 'Create':
            return federatedCreateActivityPermitted(activity, user);
        case 'Announce':
    }

    //TODO: Check to see if foreign actors are blocked or banned.
};

const processFederatedActivity = function(activity) {

    if (!activity.id || Activities.findOne({id: activity.id})) {
        console.log("Ignoring Activity: ", activity.id, activity.type);
        return;
    }

    const _id = Activities.insert(activity);

    let processFederatedActivityCallback = function(err, result) {
        try {
            federatedActivityPermitted(activity);
        } catch (error) {
            if (['Post Not Present', 'Post Already Exists'].includes(error.error)) {

                return Activities.findOne({_id: _id});
            } else throw error;
        }

        switch(activity.type) {
            case 'Follow':
                activity = processFederatedFollowActivity(activity);
                break;
            case 'Undo':
                activity = processUndoActivity(activity);
                break;
            case 'Create':
                activity = processFederatedCreateActivity(activity);
                break;
            case 'Delete':
                try {
                    activity = processFederatedDeleteActivity(activity);
                } catch (error) {
                    if (error.error !== 'Post Not Present') throw error;
                }
                break;
            case "Accept":
                activity = processAcceptActivity(activity);
                break;
            case 'Like':
                activity = processLikeActivity(activity);
                break;
            case 'Announce':
                activity = processAnnounceActivity(activity);
                break;
        }
    }

    if (activity.actor && !Actors.findOne({id: activity.actor})) { //Is this posts actor already present? If not,
        importActivityJSONFromUrl(activity.actor, processFederatedActivityCallback); //add it, and finish the function as a callback.
    } else {
        processFederatedActivityCallback(); //If it is present, then continue as normal.
    }
};

const importActorFromActivityPubJSON = function(json) {
    if (!Actors.findOne({id: json.id})) { //Is actor already present? If not,
        json.local = false; //mark it as foreign,
        Actors.insert(json); //then add it.

        console.log("Added Actor: " + json.id);
    }
};

const importPostFromActivityPubJSON = function(json) {
    if (!Posts.findOne({id: json.id})) { //Is post already present? If not,
        json.local = false; //mark it as foreign,
        Posts.insert(json); //then add it.
    }
};

importActivityJSONFromUrl = function(url, callback) {
    console.log("Importing from: " + url);
    let json = JSON.parse(HTTP.get(url, {
        headers: {
            Accept: 'application/activity+json; profile="https://www.w3.org/ns/activitystreams#"'
        }
    }).content);

    importFromActivityPubJSON(json);

    if (callback) callback(null, json);

    return json;
};

importFromActivityPubJSON = function(json) {
    if (!json) throw new Meteor.Error('Empty JSON');

    if (!json.type) throw new Meteor.Error('Untyped ActivityPub JSON');

    if (activityPubActorTypes.includes(json.type))
        importActorFromActivityPubJSON(json);

    else if (activityPubContentTypes.includes(json.type))
        importPostFromActivityPubJSON(json);
};

Meteor.methods({
    importActivityJSONFromUrl: importActivityJSONFromUrl,
    importFromActivityPubJSON: importFromActivityPubJSON,
    postActivity: function(object) {
        const user = Meteor.users.findOne({_id: this.userId});

        let result = processClientActivity(user, object);

        return result;
    }
});

//Host meta in XRD.
Router.route('/.well-known/host-meta', {
    name: "Host-Meta XRD",
    action: function () {
        let response = this.response;

        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Content-Type', 'application/xml; charset=UTF-8');

        response.end('<XRD>' +
                      '  <Link rel="lrdd"' +
                      '    type="application/xrd+xml"' +
                      '    template="' +
                      process.env.ROOT_URL + '/webfinger/xrd/{uri}" />' +
                      '</XRD>');
    },
    where: 'server'
});

//Host meta in JSON.
Router.route('/.well-known/host-meta.json', {
    name: "Host-Meta JSON",
    action: function () {
        let response = this.response;

        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Content-Type', 'application/json; charset=UTF-8');

        response.end(JSON.stringify({
            links: [
                {
                    "rel": "lrdd",
                    "type": "application/json",
                    "template": process.env.ROOT_URL + "/.well-known/webfinger?resource={uri}"
                }
            ]
        }));
    },
    where: 'server'
});

//user wbfinger profile in JSON.
Router.route('/.well-known/webfinger', {
    name: "Webfinger",
    action: function () {
        let resource = this.params.query.resource;
        var re = /(acct:@?)([^@]*)(@.*)?/;
        let handle = resource.replace(re, "$2");
        let actor = Actors.findOne({preferredUsername: handle, local: true});

        let response = this.response;

        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Content-Type', 'application/json; charset=UTF-8');

        response.end(JSON.stringify({
            subject: resource,
            aliases: [actor.url, actor.id],
            links: [
                {
                    "rel":"http://webfinger.net/rel/profile-page",
                    "type":"text/html",
                    "href": actor.url
                },
                {
                    "rel":"self",
                    "type":"application/activity+json",
                    "href": actor.id
                }
            ]
        }));
    },
    where: 'server'
});

//Post in JSON.
Router.route('/post/:_id', {
    name: "Post Data",
    action: function () {
        let post = Posts.findOne({_id: this.params._id});

        let response = this.response;

        if (this.request.headers['accept'].includes("text/html")) {
            let actor = Actors.findOne({id: post.attributedTo});
            if (post && actor && this.ready()) {
                //We're just redirecting to an html page at a different address. Kinda lazy, but it works for now.
                response.writeHead(302, {
                    'Location': '/@' + actor.preferredUsername + '/' + this.params._id
                });
                response.end();
            }
        } else {
            response.setHeader('Access-Control-Allow-Origin', '*');
            response.setHeader('Content-Type', 'activity/json; charset=UTF-8');

            response.end(JSON.stringify(cleanActivityPub(post)));
        }
    },
    where: 'server'
});

//Activity in JSON.
Router.route('/activity/:_id', {
    name: "Activity",
    action: function () {
        let activity = Activities.findOne({_id: this.params._id});

        let response = this.response;

        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Content-Type', 'activity/json; charset=UTF-8');

        response.end(JSON.stringify(cleanActivityPub(activity)));
    },
    where: 'server'
});

//Actor in JSON.
Router.route('/actor/:handle', {
    name: "Actor",
    action: function () {
        let actor = Actors.findOne({preferredUsername: this.params.handle});

        let response = this.response;

        if (this.request.headers['accept'].includes("text/html")) {
            if (actor && this.ready()) {
                //We're just redirecting to an html page at a different address. Kinda lazy, but it works for now.
                response.writeHead(302, {
                    'Location': '/@' + actor.preferredUsername
                });
                response.end();
            }
        } else {
            response.setHeader('Access-Control-Allow-Origin', '*');
            response.setHeader('Content-Type', 'activity/json; charset=UTF-8');

            response.end(JSON.stringify(cleanActivityPub(actor)));
        }
    },
    where: 'server'
});

//Outbox in JSON.
Router.route('/actor/:handle/outbox', function () {
    let outbox = Outboxes.findOne({id: process.env.ROOT_URL + "actor/" + this.params.handle + "/outbox"});

    let orderedItems = [];
    for (let i = 0; i < outbox.totalItems && i < 10; i++) {
        orderedItems.push(Activities.findOne({id: outbox.orderedItems[i]}));
    }
    outbox.orderedItems = orderedItems;

    let response = this.response;

    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Content-Type', 'activity/json; charset=UTF-8');

    response.end(JSON.stringify(cleanActivityPub(outbox)));
}, {where: 'server'});

Router.route('/actor/:handle/inbox', function () {
    let body = [];
    request.on('data', (chunk) => {
        body.push(chunk);
    }).on('end', () => {
        body = Buffer.concat(body).toString();
        processFederatedActivity(body);
    });
    this.response.end();
}, {where: 'server'});

//Following List in JSON.
Router.route('/actor/:handle/following', function () {
    let following = FollowingLists.findOne({id: process.env.ROOT_URL + "actor/" + this.params.handle + "/following"});

    let orderedItems = [];
    for (let i = 0; i < following.totalItems && i < 10; i++) {
        orderedItems.push(Actors.findOne({id: following.orderedItems[i]}));
    }
    following.orderedItems = orderedItems;

    let response = this.response;

    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Content-Type', 'activity/json; charset=UTF-8');

    response.end(JSON.stringify(cleanActivityPub(following)));
}, {where: 'server'});

//Follower List in JSON.
Router.route('/actor/:handle/followers', function () {
    let followers = FollowerLists.findOne({id: process.env.ROOT_URL + "actor/" + this.params.handle + "/followers"});

    let orderedItems = [];
    for (let i = 0; i < followers.totalItems && i < 10; i++) {
        orderedItems.push(Actors.findOne({id: followers.orderedItems[i]}));
    }
    followers.orderedItems = orderedItems;

    let response = this.response;

    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Content-Type', 'activity/json; charset=UTF-8');

    response.end(JSON.stringify(cleanActivityPub(followers)));
}, {where: 'server'});
