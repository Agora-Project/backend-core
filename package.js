Package.describe({
  name: 'agoraforum:backend-core',
  version: '0.2.2',
  // Brief, one-line summary of the package.
  summary: 'Backend for the Agora forum software.',
  // URL to the Git repository containing the source code for this package.
  git: 'https://gitlab.com/Agora-Project/backend-core',

  documentation: null,
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  //documentation: 'README.md'
  license: "LICENSE"
});


Npm.depends({
    'webfinger': '0.4.2'
});

Package.onUse(function(api) {
    api.versionsFrom('1.8.0.2');
    api.use([
        'ecmascript',
        'accounts-password',
        'agoraforum:collections-core@0.1.5',
        'agoraforum:activitypub@0.0.3',
        'alanning:roles@1.2.16',
        'iron:router@1.1.2'
    ]);

    api.use([
        'email',
        'http'
    ], 'server');

    api.addFiles([
        'lib/activityHandling/accept.js',
        'lib/activityHandling/create.js',
        'lib/activityHandling/follow.js',
        'lib/activityHandling/update.js',
        'lib/activityHandling/delete.js',
        'lib/activityHandling/like.js',
        'lib/activityHandling/undo.js',
        'lib/activityHandling/announce.js',
        'lib/clientActivity.js',
        'lib/federation.js',
        'lib/methods.js',
    ], 'server');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('agoraforum:backend-core');
  api.mainModule('backend-core-tests.js');
});
